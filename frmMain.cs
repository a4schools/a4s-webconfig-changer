using System.Xml;

namespace A4SDevWebConfigChanger;

public partial class frmMain : Form
{
    private static List<string> projectPaths = new List<string>(new[] {
        @"src\All4Teachers.WebApplications\All4Teachers.AbsenceManager.WebApp",
        @"src\All4Teachers.WebApplications\All4Teachers.Api",
        @"src\All4Teachers.WebApplications\All4Teachers.Common.WebApp",
        @"src\All4Teachers.WebApplications\All4Teachers.DocumentManager.WebApp",
        @"src\All4Teachers.WebApplications\All4Teachers.GradeManager.WebApp",
        @"src\All4Teachers.WebApplications\All4Teachers.ScheduleManager.WebApp",
        @"src\All4Teachers.WebApplications\All4Teachers.StudentAndTeacherManager.WebApp",
        @"src\All4Teachers.WebApplications\All4Teachers.WarningManager.WebApp",
    });


    public frmMain()
    {
        InitializeComponent();
        txtLog.MaxLength = int.MaxValue;
        txtProjectRoot.Text = Properties.Settings.Default.A4sRootFolder;
        chkBackup.Checked = Properties.Settings.Default.Backups;

        if (Properties.Settings.Default.ConnectionStrings == null)
        {
            Properties.Settings.Default.ConnectionStrings = new System.Collections.Specialized.StringCollection();
        }

        if (Properties.Settings.Default.DevAppUsers == null)
        {
            Properties.Settings.Default.DevAppUsers = new System.Collections.Specialized.StringCollection();
        }

        cmbConnectionStrings.Items.Clear();
        foreach (var conString in Properties.Settings.Default.ConnectionStrings)
        {
            if (!string.IsNullOrEmpty(conString))
            {
                cmbConnectionStrings.Items.Add(conString);
            }
        }

        cmbDevAppUser.Items.Clear();
        foreach (var devAppUser in Properties.Settings.Default.DevAppUsers)
        {
            if (!string.IsNullOrEmpty(devAppUser))
            {
                cmbDevAppUser.Items.Add(devAppUser);
            }
        }
    }


    private void btnBrowseFolder_Click(object sender, EventArgs e)
    {
        var result = folderBrowserDialog1.ShowDialog(this);
        if (result == DialogResult.OK)
        {
            txtProjectRoot.Text = folderBrowserDialog1.SelectedPath;
            Properties.Settings.Default.A4sRootFolder = txtProjectRoot.Text;
            Properties.Settings.Default.Save();
        }
    }


    private void Log(string message)
    {
        txtLog.AppendText($"{message}{Environment.NewLine}");
    }


    private void btnOk_Click(object sender, EventArgs e)
    {
        if (!ValidateInputs())
        {
            return;
        }

        if (!Properties.Settings.Default.ConnectionStrings.Contains(cmbConnectionStrings.Text))
        {
            Properties.Settings.Default.ConnectionStrings.Add(cmbConnectionStrings.Text);
        }

        if (!Properties.Settings.Default.DevAppUsers.Contains(cmbDevAppUser.Text))
        {
            Properties.Settings.Default.DevAppUsers.Add(cmbDevAppUser.Text);
        }

        Properties.Settings.Default.Backups = chkBackup.Checked;
        Properties.Settings.Default.Save();

        projectPaths.ForEach(projectPath =>
        {
            var absoltePath = Path.Combine(txtProjectRoot.Text, projectPath, "Web.config");
            Log($"processing {absoltePath}");
            CreateBackup(absoltePath);
            ReplaceConnectionString(absoltePath);
            ReplaceDevAppUser(absoltePath);
        });

        Log("Done...");
    }


    private void CreateBackup(string webConfigPath)
    {
        if (chkBackup.Checked && File.Exists(webConfigPath))
        {
            var backupFileName = $"{webConfigPath}.{DateTime.Now.ToString("yyyyMMdd-HHmmss")}.bak";
            while (File.Exists(backupFileName))
            {
                backupFileName = $"{webConfigPath}.{DateTime.Now.ToString("yyyyMMdd-HHmmss")}.bak";
            }

            Log($"creating backup of {webConfigPath} => {backupFileName}");
            File.Copy(webConfigPath, backupFileName, false);
        }
    }


    private void ReplaceConnectionString(string webConfigPath)
    {
        if (File.Exists(webConfigPath) && !string.IsNullOrEmpty(cmbConnectionStrings.Text))
        {
            var webConfig = new XmlDocument();

            webConfig.LoadXml(File.ReadAllText(webConfigPath));

            var conStrings = webConfig.GetElementsByTagName("connectionStrings").Item(0);
            if (conStrings != null && conStrings.ChildNodes.Count > 0)
            {
                //var nodesToRemove = new List<XmlNode>();
                foreach (XmlNode node in conStrings.ChildNodes)
                {
                    if (node.NodeType == XmlNodeType.Element)
                    {
                        if (node.Attributes != null && node.Attributes.Count > 0)
                        {
                            var attr = node.Attributes.GetNamedItem("connectionString");
                            if (attr != null)
                            {
                                Log($"replacing connection string in {webConfigPath} with {cmbConnectionStrings.Text}");
                                attr.Value = cmbConnectionStrings.Text;
                            }
                        }
                    }

                    //// todo: make optional
                    //else if (node.NodeType == XmlNodeType.Comment)
                    //{
                    //    nodesToRemove.Add(node);
                    //}
                }

                //// remove comment nodes
                //nodesToRemove.ForEach(node => conStrings.RemoveChild(node));

                webConfig.Save(webConfigPath);
            }
        }
    }


    private void ReplaceDevAppUser(string webConfigPath)
    {
        if (File.Exists(webConfigPath) && !string.IsNullOrEmpty(cmbDevAppUser.Text))
        {
            var webConfig = new XmlDocument();

            webConfig.LoadXml(File.ReadAllText(webConfigPath));

            var appSettings = webConfig.GetElementsByTagName("All4Teachers.Api.Properties.Settings").Item(0);
            if (appSettings != null && appSettings.ChildNodes.Count > 0)
            {
                foreach (XmlNode node in appSettings.ChildNodes)
                {
                    if (node.NodeType == XmlNodeType.Element
                        && node.HasChildNodes
                        && node.Attributes != null
                        && node.Attributes.Count > 0)
                    {
                        var settingsNameAttr = node.Attributes.GetNamedItem("name");
                        if (settingsNameAttr != null && settingsNameAttr.Value == "DevAppUserForAuthentication")
                        {
                            // find value node
                            var valueNode = node.ChildNodes.Cast<XmlNode>()
                                                           .FirstOrDefault(x => x.NodeType == XmlNodeType.Element && x.Name == "value");
                            if (valueNode != null)
                            {
                                Log($"replacing DevAppUser in {webConfigPath} with: {cmbDevAppUser.Text}");
                                valueNode.InnerText = cmbDevAppUser.Text;
                            }
                        }
                    }
                }

                webConfig.Save(webConfigPath);
            }
        }
    }


    private bool ValidateInputs()
    {
        if (string.IsNullOrEmpty(cmbDevAppUser.Text) && string.IsNullOrEmpty(cmbConnectionStrings.Text))
        {
            MessageBox.Show(this, "Insert/Select a ConnectionString and/or DevAppUser", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            return false;
        }
        return true;
    }

}
