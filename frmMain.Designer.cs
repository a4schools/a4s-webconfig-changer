﻿namespace A4SDevWebConfigChanger
{
    partial class frmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            txtProjectRoot = new TextBox();
            btnBrowseFolder = new Button();
            folderBrowserDialog1 = new FolderBrowserDialog();
            cmbConnectionStrings = new ComboBox();
            btnOk = new Button();
            label1 = new Label();
            label2 = new Label();
            chkBackup = new CheckBox();
            txtLog = new TextBox();
            tableLayoutPanel1 = new TableLayoutPanel();
            tableLayoutPanel2 = new TableLayoutPanel();
            tableLayoutPanel3 = new TableLayoutPanel();
            tableLayoutPanel4 = new TableLayoutPanel();
            label3 = new Label();
            cmbDevAppUser = new ComboBox();
            tableLayoutPanel5 = new TableLayoutPanel();
            tableLayoutPanel1.SuspendLayout();
            tableLayoutPanel2.SuspendLayout();
            tableLayoutPanel3.SuspendLayout();
            tableLayoutPanel4.SuspendLayout();
            tableLayoutPanel5.SuspendLayout();
            SuspendLayout();
            // 
            // txtProjectRoot
            // 
            txtProjectRoot.Dock = DockStyle.Fill;
            txtProjectRoot.Location = new Point(3, 3);
            txtProjectRoot.Name = "txtProjectRoot";
            txtProjectRoot.Size = new Size(642, 23);
            txtProjectRoot.TabIndex = 0;
            // 
            // btnBrowseFolder
            // 
            btnBrowseFolder.Location = new Point(651, 3);
            btnBrowseFolder.Name = "btnBrowseFolder";
            btnBrowseFolder.Size = new Size(32, 23);
            btnBrowseFolder.TabIndex = 1;
            btnBrowseFolder.Text = "...";
            btnBrowseFolder.UseVisualStyleBackColor = true;
            btnBrowseFolder.Click += btnBrowseFolder_Click;
            // 
            // cmbConnectionStrings
            // 
            cmbConnectionStrings.AllowDrop = true;
            cmbConnectionStrings.Dock = DockStyle.Fill;
            cmbConnectionStrings.FormattingEnabled = true;
            cmbConnectionStrings.Items.AddRange(new object[] { "<add name=\"DataAccessConnectionString\" connectionString=\"Data Source=172.17.0.1,1433;Initial Catalog=a4s_phorms;User ID=SA;Password=123;Connection Timeout=120;MultipleActiveResultSets=True;\" />", "<add name=\"DataAccessConnectionString\" connectionString=\"Data Source=172.17.0.1,1433;Initial Catalog=a4s_testnoten;User ID=SA;Password=123;Connection Timeout=120;MultipleActiveResultSets=True;\" />" });
            cmbConnectionStrings.Location = new Point(13, 81);
            cmbConnectionStrings.Name = "cmbConnectionStrings";
            cmbConnectionStrings.Size = new Size(680, 23);
            cmbConnectionStrings.TabIndex = 2;
            // 
            // btnOk
            // 
            btnOk.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnOk.Location = new Point(118, 30);
            btnOk.Name = "btnOk";
            btnOk.Size = new Size(75, 23);
            btnOk.TabIndex = 3;
            btnOk.Text = "Go";
            btnOk.UseVisualStyleBackColor = true;
            btnOk.Click += btnOk_Click;
            // 
            // label1
            // 
            label1.Location = new Point(13, 10);
            label1.Name = "label1";
            label1.Size = new Size(112, 15);
            label1.TabIndex = 4;
            label1.Text = "Project-Root-Folder";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(13, 60);
            label2.Name = "label2";
            label2.Size = new Size(105, 15);
            label2.TabIndex = 5;
            label2.Text = "Connection-String";
            // 
            // chkBackup
            // 
            chkBackup.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            chkBackup.AutoSize = true;
            chkBackup.Checked = true;
            chkBackup.CheckState = CheckState.Checked;
            chkBackup.Location = new Point(86, 3);
            chkBackup.Name = "chkBackup";
            chkBackup.Size = new Size(107, 19);
            chkBackup.TabIndex = 6;
            chkBackup.Text = "Create Backups";
            chkBackup.UseVisualStyleBackColor = true;
            // 
            // txtLog
            // 
            txtLog.Dock = DockStyle.Fill;
            txtLog.Font = new Font("Courier New", 9F, FontStyle.Regular, GraphicsUnit.Point);
            txtLog.Location = new Point(13, 170);
            txtLog.Multiline = true;
            txtLog.Name = "txtLog";
            txtLog.ReadOnly = true;
            txtLog.ScrollBars = ScrollBars.Both;
            txtLog.Size = new Size(680, 170);
            txtLog.TabIndex = 7;
            txtLog.WordWrap = false;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Controls.Add(label1, 0, 0);
            tableLayoutPanel1.Controls.Add(tableLayoutPanel2, 0, 1);
            tableLayoutPanel1.Controls.Add(txtLog, 0, 5);
            tableLayoutPanel1.Controls.Add(label2, 0, 2);
            tableLayoutPanel1.Controls.Add(cmbConnectionStrings, 0, 3);
            tableLayoutPanel1.Controls.Add(tableLayoutPanel3, 0, 4);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.Padding = new Padding(10);
            tableLayoutPanel1.RowCount = 6;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 18F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 32F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 18F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 32F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 57F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Size = new Size(706, 353);
            tableLayoutPanel1.TabIndex = 9;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.ColumnCount = 2;
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel2.Controls.Add(txtProjectRoot, 0, 0);
            tableLayoutPanel2.Controls.Add(btnBrowseFolder, 1, 0);
            tableLayoutPanel2.Dock = DockStyle.Fill;
            tableLayoutPanel2.Location = new Point(10, 28);
            tableLayoutPanel2.Margin = new Padding(0);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 1;
            tableLayoutPanel2.RowStyles.Add(new RowStyle());
            tableLayoutPanel2.Size = new Size(686, 32);
            tableLayoutPanel2.TabIndex = 5;
            // 
            // tableLayoutPanel3
            // 
            tableLayoutPanel3.ColumnCount = 2;
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 196F));
            tableLayoutPanel3.Controls.Add(tableLayoutPanel4, 0, 0);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel5, 1, 0);
            tableLayoutPanel3.Dock = DockStyle.Fill;
            tableLayoutPanel3.Location = new Point(10, 110);
            tableLayoutPanel3.Margin = new Padding(0);
            tableLayoutPanel3.Name = "tableLayoutPanel3";
            tableLayoutPanel3.RowCount = 1;
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel3.Size = new Size(686, 57);
            tableLayoutPanel3.TabIndex = 6;
            // 
            // tableLayoutPanel4
            // 
            tableLayoutPanel4.ColumnCount = 1;
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel4.Controls.Add(label3, 0, 0);
            tableLayoutPanel4.Controls.Add(cmbDevAppUser, 0, 1);
            tableLayoutPanel4.Dock = DockStyle.Fill;
            tableLayoutPanel4.Location = new Point(0, 0);
            tableLayoutPanel4.Margin = new Padding(0);
            tableLayoutPanel4.Name = "tableLayoutPanel4";
            tableLayoutPanel4.RowCount = 2;
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Absolute, 18F));
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel4.Size = new Size(490, 57);
            tableLayoutPanel4.TabIndex = 9;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(3, 0);
            label3.Name = "label3";
            label3.Size = new Size(72, 15);
            label3.TabIndex = 0;
            label3.Text = "DevAppUser";
            // 
            // cmbDevAppUser
            // 
            cmbDevAppUser.AllowDrop = true;
            cmbDevAppUser.Dock = DockStyle.Top;
            cmbDevAppUser.FormattingEnabled = true;
            cmbDevAppUser.Items.AddRange(new object[] { "<add name=\"DataAccessConnectionString\" connectionString=\"Data Source=172.17.0.1,1433;Initial Catalog=a4s_phorms;User ID=SA;Password=123;Connection Timeout=120;MultipleActiveResultSets=True;\" />", "<add name=\"DataAccessConnectionString\" connectionString=\"Data Source=172.17.0.1,1433;Initial Catalog=a4s_testnoten;User ID=SA;Password=123;Connection Timeout=120;MultipleActiveResultSets=True;\" />" });
            cmbDevAppUser.Location = new Point(3, 21);
            cmbDevAppUser.Name = "cmbDevAppUser";
            cmbDevAppUser.Size = new Size(484, 23);
            cmbDevAppUser.TabIndex = 8;
            // 
            // tableLayoutPanel5
            // 
            tableLayoutPanel5.ColumnCount = 1;
            tableLayoutPanel5.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel5.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
            tableLayoutPanel5.Controls.Add(btnOk, 0, 1);
            tableLayoutPanel5.Controls.Add(chkBackup, 0, 0);
            tableLayoutPanel5.Dock = DockStyle.Fill;
            tableLayoutPanel5.Location = new Point(490, 0);
            tableLayoutPanel5.Margin = new Padding(0);
            tableLayoutPanel5.Name = "tableLayoutPanel5";
            tableLayoutPanel5.RowCount = 2;
            tableLayoutPanel5.RowStyles.Add(new RowStyle(SizeType.Percent, 47.36842F));
            tableLayoutPanel5.RowStyles.Add(new RowStyle(SizeType.Percent, 52.63158F));
            tableLayoutPanel5.Size = new Size(196, 57);
            tableLayoutPanel5.TabIndex = 10;
            // 
            // frmMain
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(706, 353);
            Controls.Add(tableLayoutPanel1);
            Name = "frmMain";
            Text = "A4S WebConfig Changer";
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
            tableLayoutPanel2.ResumeLayout(false);
            tableLayoutPanel2.PerformLayout();
            tableLayoutPanel3.ResumeLayout(false);
            tableLayoutPanel4.ResumeLayout(false);
            tableLayoutPanel4.PerformLayout();
            tableLayoutPanel5.ResumeLayout(false);
            tableLayoutPanel5.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private TextBox txtProjectRoot;
        private Button btnBrowseFolder;
        private FolderBrowserDialog folderBrowserDialog1;
        private ComboBox cmbConnectionStrings;
        private Button btnOk;
        private Label label1;
        private Label label2;
        private CheckBox chkBackup;
        private TextBox txtLog;
        private TableLayoutPanel tableLayoutPanel1;
        private TableLayoutPanel tableLayoutPanel2;
        private TableLayoutPanel tableLayoutPanel3;
        private ComboBox cmbDevAppUser;
        private TableLayoutPanel tableLayoutPanel4;
        private Label label3;
        private TableLayoutPanel tableLayoutPanel5;
    }
}